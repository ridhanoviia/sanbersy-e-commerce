<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/admin', function () {
    return view('dashboardd');
});


Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'ProductController@welcome');
Route::get('/coba', 'ProductController@coba')->name('coba');

Route::middleware('role:admin')->get('/dashboard', 'HomeController@index2')->name('dashboard');
Route::middleware('role:admin')->get('/admin/createproduct', 'ProductController@create')->name('p_create');
Route::middleware('role:admin')->get('/admin/dataproduct', 'ProductController@index')->name('p_edit');
Route::middleware('role:admin')->post('/dashboard', 'ProductController@store');
Route::middleware('role:admin')->get('/admin/dataproduct/{id}', 'ProductController@show');
Route::middleware('role:admin')->get('/admin/dataproduct/{id}/edit', 'ProductController@edit');
Route::middleware('role:admin')->put('/admin/dataproduct/{id}', 'ProductController@update');
Route::middleware('role:admin')->delete('/admin/dataproduct/{id}', 'ProductController@destroy');

Route::middleware('role:admin')->get('/admin/datauser', 'UserController@index')->name('u_edit');
Route::middleware('role:admin')->get('/admin/datauser/{id}', 'UserController@show');
Route::middleware('role:admin')->delete('/admin/datauser/{id}', 'UserController@destroy');

Route::get('/profile', 'HomeController@profile');
Route::post('/profile', 'HomeController@update');
Route::get('/detail/{id}', 'ProductController@detail');
Route::post('/detail/{id}', 'ProductController@cart');


// Route::get('/dashboard', function () {
//     return view('dashboard');
// });
