<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Forms - Azzara Bootstrap 4 Admin Dashboard</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{asset('assets/img/icon.ico')}}" type="image/x-icon"/>
	
	<!-- Fonts and icons -->
	<script src="{{asset('assets/js/plugin/webfont/webfont.min.js')}}"></script>
	<script>
		WebFont.load({
			google: {"families":["Open+Sans:300,400,600,700"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands"], urls: ['../../../assets/css/fonts.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/azzara.min.css')}}">
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<!-- <link rel="stylesheet" href="../../assets/css/demo.css"> -->
</head>
<body>
	<div class="wrapper">
		<!--
				Tip 1: You can change the background color of the main header using: data-background-color="blue | purple | light-blue | green | orange | red"
		-->
		<div class="main-header" data-background-color="purple">
			<!-- Logo Header -->
			<div class="logo-header">
				
				<a href="../index.html" class="logo">
					<span alt="navbar brand" class="navbar-brand" style="color: #fff">Male Fashion</span>
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="fa fa-bars"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="fa fa-ellipsis-v"></i></button>
				<div class="navbar-minimize">
					<button class="btn btn-minimize btn-rounded">
						<i class="fa fa-bars"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg" style="background-color: black;">
				
				<div class="container-fluid">
					<div class="collapse" id="search-nav">
						<form class="navbar-left navbar-form nav-search mr-md-3">
							<div class="input-group">
								<div class="input-group-prepend">
									<button type="submit" class="btn btn-search pr-1">
										<i class="fa fa-search search-icon"></i>
									</button>
								</div>
								<input type="text" placeholder="Search ..." class="form-control">
							</div>
						</form>
					</div>
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item toggle-nav-search hidden-caret">
							<a class="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
								<i class="fa fa-search"></i>
							</a>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-bell"></i>
								<span class="notification">4</span>
							</a>
							<ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
								<li>
									<div class="dropdown-title">You have 4 new notification</div>
								</li>
								<li>
									<div class="notif-center">
										<a href="#">
											<div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
											<div class="notif-content">
												<span class="block">
													New user registered
												</span>
												<span class="time">5 minutes ago</span> 
											</div>
										</a>
										<a href="#">
											<div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
											<div class="notif-content">
												<span class="block">
													Rahmad commented on Admin
												</span>
												<span class="time">12 minutes ago</span> 
											</div>
										</a>
										<a href="#">
											<div class="notif-img"> 
												<img src="{{asset('assets/img/profile2.jpg')}}" alt="Img Profile">
											</div>
											<div class="notif-content">
												<span class="block">
													Reza send messages to you
												</span>
												<span class="time">12 minutes ago</span> 
											</div>
										</a>
										<a href="#">
											<div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
											<div class="notif-content">
												<span class="block">
													Farrah liked Admin
												</span>
												<span class="time">17 minutes ago</span> 
											</div>
										</a>
									</div>
								</li>
								<li>
									<a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
								</li>
							</ul>
						</li>
						<li class="nav-item dropdown hidden-caret">
							<div class="dropdown-toggle profile-pic" data-toggle="dropdown" aria-expanded="false">
								<div class="avatar-sm">
									<img src="{{asset('assets/img/profile.jpg')}}" alt="..." class="avatar-img rounded-circle">
								</div>
							</div>
						</li>
						
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>
		<!-- Sidebar -->
		<div class="sidebar">
			
			<div class="sidebar-wrapper scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="{{asset('assets/img/profile.jpg')}}" alt="..." class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									{{ Auth::user()->name }}
									<span class="user-level">{{ Auth::user()->email }}</span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="#profile">
											<span class="link-collapse">My Profile</span>
										</a>
									</li>
									<li>
										<a href="{{ url('/home') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav">
						<li class="nav-item active">
							<a href="../index.html">
								<i class="fas fa-home"></i>
								<p>Dashboard</p>
								{{-- <span class="badge badge-count">5</span> --}}
							</a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Components</h4>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#base">
								<i class="fas fa-layer-group"></i>
								<p>Product</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="base">
								<ul class="nav nav-collapse">
									<li>
										<a href="{{route('p_edit')}}">
											<span class="sub-item">All Data</span>
										</a>
									</li>
									<li>
										<a href="{{route('p_create')}}">
											<span class="sub-item">Create Data</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
						<li class="nav-item ">
							<a data-toggle="collapse" href="#user">
								<i class="fas fa-pen-square"></i>
								<p>User</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="form">
								<ul class="nav nav-collapse">
									<li class="active">
										<a href="{{route('u_edit')}}">
											<span class="sub-item">All User</span>
										</a>
									</li>
                                    {{-- <li class="active">
										<a href="../forms/forms.html">
											<span class="sub-item">Create User</span>
										</a>
									</li> --}}
									
								</ul>
							</div>
						</li>
						<li class="nav-item">
							<a data-toggle="collapse" href="#tables">
								<i class="fas fa-table"></i>
								<p>Transaction</p>
								<span class="caret"></span>
							</a>
							<div class="collapse" id="tables">
								<ul class="nav nav-collapse">
									<li>
										<a href="../tables/tables.html">
											<span class="sub-item">History</span>
										</a>
									</li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
        {{-- Main --}}
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
                    @yield('title')
					{{-- <div class="page-header">
						<h4 class="page-title">Forms</h4>
						<ul class="breadcrumbs">
							<li class="nav-home">
								<a href="#">
									<i class="flaticon-home"></i>
								</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Forms</a>
							</li>
							<li class="separator">
								<i class="flaticon-right-arrow"></i>
							</li>
							<li class="nav-item">
								<a href="#">Basic Form</a>
							</li>
						</ul>
					</div> --}}
                    @yield('main')
					{{-- <div class="row">
						<div class="col-md-6">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Base Form Control</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label for="email2">Email Address</label>
										<input type="email" class="form-control" id="email2" placeholder="Enter Email">
										<small id="emailHelp2" class="form-text text-muted">We'll never share your email with anyone else.</small>
									</div>
									<div class="form-group">
										<label for="password">Password</label>
										<input type="password" class="form-control" id="password" placeholder="Password">
									</div>
									<div class="form-group form-inline">
										<label for="inlineinput" class="col-md-3 col-form-label">Inline Input</label>
										<div class="col-md-9 p-0">
											<input type="text" class="form-control input-full" id="inlineinput" placeholder="Enter Input">
										</div>
									</div>
									<div class="form-group has-success">
										<label for="successInput">Success Input</label>
										<input type="text" id="successInput" value="Success" class="form-control">
									</div>
									<div class="form-group has-error has-feedback">
										<label for="errorInput">Error Input</label>
										<input type="text" id="errorInput" value="Error" class="form-control">
										<small id="emailHelp" class="form-text text-muted">Please provide a valid informations.</small>
									</div>
									<div class="form-group">
										<label for="disableinput">Disable Input</label>
										<input type="text" class="form-control" id="disableinput" placeholder="Enter Input" disabled>
									</div>
									<div class="form-check">
										<label>Gender</label><br/>
										<label class="form-radio-label">
											<input class="form-radio-input" type="radio" name="optionsRadios" value=""  checked="">
											<span class="form-radio-sign">Male</span>
										</label>
										<label class="form-radio-label ml-3">
											<input class="form-radio-input" type="radio" name="optionsRadios" value="">
											<span class="form-radio-sign">Female</span>
										</label>
									</div>
									<div class="form-group">
										<label class="control-label">
											Static
										</label> 
										<p class="form-control-static">hello@example.com</p> 
									</div>
									<div class="form-group">
										<label for="exampleFormControlSelect1">Example select</label>
										<select class="form-control" id="exampleFormControlSelect1">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="form-group">
										<label for="exampleFormControlSelect2">Example multiple select</label>
										<select multiple class="form-control" id="exampleFormControlSelect2">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="form-group">
										<label for="exampleFormControlFile1">Example file input</label>
										<input type="file" class="form-control-file" id="exampleFormControlFile1">
									</div>
									<div class="form-group">
										<label for="comment">Comment</label>
										<textarea class="form-control" id="comment" rows="5">

										</textarea>
									</div>
									<div class="form-check">
										<label class="form-check-label">
											<input class="form-check-input" type="checkbox" value="">
											<span class="form-check-sign">Agree with terms and conditions</span>
										</label>
									</div>
								</div>
								<div class="card-action">
									<button class="btn btn-success">Submit</button>
									<button class="btn btn-danger">Cancel</button>
								</div>
							</div>
							<div class="card">
								<div class="card-header">
									<div class="card-title">Input Group</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1">@</span>
											</div>
											<input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
											<input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
											<div class="input-group-append">
												<span class="input-group-text" id="basic-addon2">@example.com</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="basic-url">Your vanity URL</label>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon3">https://example.com/users/</span>
											</div>
											<input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<span class="input-group-text">$</span>
											</div>
											<input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
											<div class="input-group-append">
												<span class="input-group-text">.00</span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text">With textarea</span>
											</div>
											<textarea class="form-control" aria-label="With textarea"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<div class="input-group-prepend">
												<button class="btn btn-default btn-border" type="button">Button</button>
											</div>
											<input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">
										</div>
									</div>
									<div class="form-group">
										<div class="input-group">
											<input type="text" class="form-control" aria-label="Text input with dropdown button">
											<div class="input-group-append">
												<button class="btn btn-primary btn-border dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="#">Action</a>
													<a class="dropdown-item" href="#">Another action</a>
													<a class="dropdown-item" href="#">Something else here</a>
													<div role="separator" class="dropdown-divider"></div>
													<a class="dropdown-item" href="#">Separated link</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="card-action">
									<button class="btn btn-success">Submit</button>
									<button class="btn btn-danger">Cancel</button>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="card">
								<div class="card-header">
									<div class="card-title">Form Group Styles</div>
								</div>
								<div class="card-body">
									<label class="mb-3"><b>Form Group Default</b></label>
									<div class="form-group form-group-default">
										<label>Input</label>
										<input id="Name" type="text" class="form-control" placeholder="Fill Name">
									</div>
									<div class="form-group form-group-default">
										<label>Select</label>
										<select class="form-control" id="formGroupDefaultSelect">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<label class="mt-3 mb-3"><b>Form Floating Label</b></label>
									<div class="form-group form-floating-label">
										<input id="inputFloatingLabel" type="text" class="form-control input-border-bottom" required>
										<label for="inputFloatingLabel" class="placeholder">Input</label>
									</div>
									<div class="form-group form-floating-label">
										<select class="form-control input-border-bottom" id="selectFloatingLabel" required>
											<option value="">&nbsp;</option>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
										<label for="selectFloatingLabel" class="placeholder">Select</label>
									</div>
									<div class="form-group form-floating-label">
										<input id="inputFloatingLabel2" type="text" class="form-control input-solid" required>
										<label for="inputFloatingLabel2" class="placeholder">Input</label>
									</div>
									<div class="form-group form-floating-label">
										<select class="form-control input-solid" id="selectFloatingLabel2" required>
											<option value="">&nbsp;</option>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
										<label for="selectFloatingLabel2" class="placeholder">Select</label>
									</div>
								</div>
							</div>
							<div class="card">
								<div class="card-header">
									<div class="card-title">Form Control Styles</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label for="squareInput">Square Input</label>
										<input type="text" class="form-control input-square" id="squareInput" placeholder="Square Input">
									</div>
									<div class="form-group">
										<label for="squareSelect">Square Select</label>
										<select class="form-control input-square" id="squareSelect">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="form-group">
										<label for="pillInput">Pill Input</label>
										<input type="text" class="form-control input-pill" id="pillInput" placeholder="Pill Input">
									</div>
									<div class="form-group">
										<label for="pillSelect">Pill Select</label>
										<select class="form-control input-pill" id="pillSelect">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="form-group">
										<label for="solidInput">Solid Input</label>
										<input type="text" class="form-control input-solid" id="solidInput" placeholder="Solid Input">
									</div>
									<div class="form-group">
										<label for="solidSelect">Solid Select</label>
										<select class="form-control input-solid" id="solidSelect">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>											
								</div>
								<div class="card-action">
									<button class="btn btn-success">Submit</button>
									<button class="btn btn-danger">Cancel</button>
								</div>
							</div>
							<div class="card">
								<div class="card-header">
									<div class="card-title">Form Control Styles</div>
								</div>
								<div class="card-body">
									<div class="form-group">
										<label for="largeInput">Large Input</label>
										<input type="text" class="form-control form-control-lg" id="largeInput" placeholder="Large Input">
									</div>
									<div class="form-group">
										<label for="largeInput">Default Input</label>
										<input type="text" class="form-control form-control" id="defaultInput" placeholder="Default Input">
									</div>
									<div class="form-group">
										<label for="smallInput">Small Input</label>
										<input type="text" class="form-control form-control-sm" id="smallInput" placeholder="Small Input">
									</div>
									<div class="form-group">
										<label for="largeSelect">Large Select</label>
										<select class="form-control form-control-lg" id="largeSelect">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="form-group">
										<label for="defaultSelect">Default Select</label>
										<select class="form-control form-control" id="defaultSelect">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
									<div class="form-group">
										<label for="smallSelect">Small Select</label>
										<select class="form-control form-control-sm" id="smallSelect">
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
								</div>
								<div class="card-action">
									<button class="btn btn-success">Submit</button>
									<button class="btn btn-danger">Cancel</button>
								</div>
							</div>
						</div>
					</div> --}}
				</div>
			</div>
			
		</div>
		
		
	</div>
	<!--   Core JS Files   -->
	<script src="{{asset('assets/js/core/jquery.3.2.1.min.js')}}"></script>
	<script src="{{asset('assets/js/core/popper.min.js')}}"></script>
	<script src="{{asset('assets/js/core/bootstrap.min.js')}}"></script>
	<!-- jQuery UI -->
	<script src="{{asset('assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script src="{{asset('assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js')}}"></script>
	<!-- Bootstrap Toggle -->
	<script src="{{asset('assets/js/plugin/bootstrap-toggle/bootstrap-toggle.min.js')}}"></script>
	<!-- jQuery Scrollbar -->
	<script src="{{asset('assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
	<!-- Azzara JS -->
	<script src="{{asset('assets/js/ready.min.js')}}"></script>
	<!-- Azzara DEMO methods, don't include it in your project! -->
	<script src="{{('assets/js/setting-demo.js')}}"></script>
	@include('sweetalert::alert')

</body>
</html>