@extends('dashboard')

@section('title')
<div class="page-header">
    <h4 class="page-title">Data User</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <p>User</p>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="/admin/dataproduct">All Data</a>
        </li>
    </ul>
</div>
@endsection
@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">All Data</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user as $key => $user)
                            <tr>
                                @if ($user->id != 1)
                                    <td> {{ $key }} </td>
                                    <td> {{ $user -> name }} </td>
                                    <td> {{ $user -> email }} </td>
                                    <td> 
                                        <a href="/admin/datauser/{{$user->id}}" data-toggle="tooltip" title="" class="btn btn-link btn-warning" data-original-title="Show Data">
                                            <i class=" fa fas fa-folder-open"></i>
                                        </a>
                                        <form action="/admin/datauser/{{$user->id}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                        <input type="submit" value="delete" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove">
                                        </form>
                                    </td>       
                                        
                                @endif
                                    {{-- <form action="/admin/dataproduct/{{$product->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/admin/dataproduct/{{$product->id}}/edit" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <input type="submit" value="delete" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove">
                                    </form>  --}}
                                    {{-- <a href="/admin/dataproduct/{{$product->id}}" value type="submit" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
                                        <i class="fa fa-times"></i>
                                    </a> --}}
                                  
                                    {{-- <a href="/admin/dataproduct/{{$product->id}}" class="badge success">show</a>
                                    <a href="/admin/dataproduct/{{$product->id}}/edit" class="badge success">edit</a> --}}
                                    {{-- <a href="/admin/dataproduct/{{$product->id}}" class="badge success">delete</a> --}}
                                    {{-- <a href="/admin/dataalbum/{{$album->id}}/edit" class="btn btn-default btn-sm">edit</a>  --}}
                                    {{-- <form action="/admin/dataproduct/{{$product->id}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                        <input type="submit" value="delete" class="badge success">
                                    </form>  --}}
                            </tr>
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
