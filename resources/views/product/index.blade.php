@extends('dashboard')

@section('title')
<div class="page-header">
    <h4 class="page-title">Data Product</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <p>Product</p>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="/admin/dataproduct">All Data</a>
        </li>
    </ul>
</div>
@endsection
@section('main')
@if (session('success_message'))
    <div class="alert alert-success">
        {{session('success_message')}}
    </div>
    
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">All Data</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Product Name</th>
                                <th>Brand</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Gambar</th>
                                <th>Deskripsi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $key => $product)
                            <tr>
                                <td> {{ $key + 1 }} </td>
                                <td> {{ $product -> name }} </td>
                                <td> {{ $product -> brand }} </td>
                                <td> {{ $product -> jumlah }} </td>
                                <td> Rp. {{ $product -> harga }} </td>
                                <td> <img class="card-img-top" src="/image/{{ $product -> gambar }}" style="width:100px"> </td>
                                <td> {{ $product -> deskripsi }} </td>
                                <td>
                                    <form action="/admin/dataproduct/{{$product->id}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/admin/dataproduct/{{$product->id}}" data-toggle="tooltip" title="" class="btn btn-link btn-warning" data-original-title="Show Task">
                                            <i class=" fa fas fa-folder-open"></i>
                                        </a>
                                        <a href="/admin/dataproduct/{{$product->id}}/edit" data-toggle="tooltip" title="" class="btn btn-link btn-primary btn-lg" data-original-title="Edit Task">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        {{-- <a href="/admin/dataproduct/{{$product->id}}" value type="submit" data-toggle="tooltip" title="" class="btn btn-link btn-danger" data-original-title="Remove">
                                            <i class="fa fa-times"></i>
                                        </a> --}}
                                            <input type="submit" value="delete" data-toggle="tooltip" class="btn btn-danger" data-original-title="Remove">
                                     </form> 
                                  
                                    {{-- <a href="/admin/dataproduct/{{$product->id}}" class="badge success">show</a>
                                    <a href="/admin/dataproduct/{{$product->id}}/edit" class="badge success">edit</a> --}}
                                    {{-- <a href="/admin/dataproduct/{{$product->id}}" class="badge success">delete</a> --}}
                                    {{-- <a href="/admin/dataalbum/{{$album->id}}/edit" class="btn btn-default btn-sm">edit</a>  --}}
                                    {{-- <form action="/admin/dataproduct/{{$product->id}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                        <input type="submit" value="delete" class="badge success">
                                    </form>  --}}
                                </td>       
                            </tr>
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
