@extends('dashboard')

@section('title')
<div class="page-header">
    <h4 class="page-title">Data Product</h4>
    <ul class="breadcrumbs">
        <li class="nav-home">
            <a href="#">
                <i class="flaticon-home"></i>
            </a>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <p>Product</p>
        </li>
        <li class="separator">
            <i class="flaticon-right-arrow"></i>
        </li>
        <li class="nav-item">
            <a href="/admin/dataproduct/{id}">Show Data</a>
        </li>
    </ul>
</div>
@endsection
@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Show Data Product ID : {{$product->id}}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover" >
                        <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Brand</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Gambar</th>
                                <th>Deskripsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> {{ $product -> name }} </td>
                                <td> {{ $product -> brand }} </td>
                                <td> {{ $product -> jumlah }} </td>
                                <td> {{ $product -> harga }} </td>
                                <td> <img class="card-img-top" src="../../image/{{ $product -> gambar }}" style="width:100px"> </td>
                                <td> {{ $product -> deskripsi }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
