@extends('dashboard')

@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Create Data Product</div>
            </div>
            <form role="form" action="/dashboard" method="POST" enctype="multipart/form-data">
            @csrf
                <div class="card-body">
                    <div class="row mb-6">
                        <div class="col-lg-6">
                            <div class="form-group ">
                                <label for="name">Product Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter The Product Name">
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="brand">Brand</label>
                                <input type="text" class="form-control" id="brand" name="brand" placeholder="The Product's Brand">
                            </div>
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="harga">Price</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    <input type="text" class="form-control" id="harga" name="harga" placeholder="000">
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="jumlah">Stock</label>
                                <input id="inputFloatingLabel" type="number" class="form-control input-border-bottom" id="jumlah" name="jumlah" min="0" value="0">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row mb-6">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="gambar">Product Image</label>
                                <input type="file" class="form-control-file" id="gambar" name="gambar">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="deskripsi">Description</label>
                        <textarea class="form-control" id="deskripsi" name="deskripsi" rows="5">
                            
                        </textarea>
                    </div>
                    
                    <div class="card-action" style="text-align: center">
                        <button class="btn btn-success">Submit</button>
                    </div>
                
                </div>
            </form>
            
        </div>
        
    </div>
   
</div> 
    {{-- <section class="create-product">
        <div class="col-md-12">
            <div class="card">
                <form role="form" action="/dashboard" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 text-right control-label col-form-label">Product Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Product Name">
                            </div>
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group row">
                            <label for="brand" class="col-sm-2 text-right control-label col-form-label">Brand</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="brand" name="brand" placeholder="Brand Name">
                            </div>
                        @error('brand')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group row">
                            <label for="jumlah" class="col-sm-2 text-right control-label col-form-label">Jumlah Product</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="">
                            </div>
                        @error('jumlah')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group row">
                            <label for="harga" class="col-sm-2 text-right control-label col-form-label">Harga</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="harga" name="harga" placeholder="..."  aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">Rp</span>
                                    </div>
                                </div>
                            </div>
                        @error('Harga')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group row">
                            <label for="gambar" class="col-sm-2 text-right control-label col-form-label">Gambar</label>
                            <div class="col-sm-9">
                                <input type="file" name="gambar">
                            </div>
                        @error('gambar')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-2 text-right control-label col-form-label">Deskripsi</label>
                            <div class="col-sm-9">
                                <textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="Deskripsi Product"></textarea>
                            </div>
                        @error('deskripsi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        </div>
                    </div>
                    <!-- /.card-body -->
                
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </section> --}}
@endsection