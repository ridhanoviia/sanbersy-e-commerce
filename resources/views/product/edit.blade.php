@extends('dashboard')

@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">Edit Data Product ID : {{$product->id}}</div>
            </div>
            <form role="form" action="/admin/dataproduct/{{$product->id}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
                <div class="card-body">
                    <div class="row mb-6">
                        <div class="col-lg-6">
                            <div class="form-group ">
                                <label for="name">Product Name</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{$product->name}}" placeholder="Enter The Product Name">
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="brand">Brand</label>
                                <input type="text" class="form-control" id="brand" name="brand" value="{{$product->brand}}" placeholder="The Product's Brand">
                            </div>
                        </div>
                    </div>

                    <div class="row mb-6">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="harga">Price</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp</span>
                                    </div>
                                    <input type="text" class="form-control" id="harga" name="harga" value="{{$product->harga}}" placeholder="000">
                                    <div class="input-group-append">
                                        <span class="input-group-text">.00</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="jumlah">Stock</label>
                                <input id="inputFloatingLabel" type="number" class="form-control input-border-bottom" id="jumlah" name="jumlah" value="{{$product->jumlah}}" min="0" value="0">
                            </div>
                        </div>
                    </div>
                    
                    <div class="row mb-6">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="gambar">Product Image</label>
                                <input type="file" class="form-control-file" id="gambar" name="gambar" value="{{$product->gambar}}">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="deskripsi">Description</label>
                        <textarea class="form-control" id="deskripsi" name="deskripsi" value="{{$product->deskripsi}}" rows="5">
                            
                        </textarea>
                    </div>
                    
                    <div class="card-action" style="text-align: center">
                        <button class="btn btn-success">Submit</button>
                    </div>
                
                </div>
            </form>
            
        </div>
        
    </div>
   
</div> 
@endsection