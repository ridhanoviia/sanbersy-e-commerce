@extends('dashboard')

@section('title')
    <div class="page-header">
        <h4 class="page-title">Dashboard</h4>
    </div>
@endsection

@section('main')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Data Product</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover" >
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Product Name</th>
                                <th>ID</th>
                                <th>Brand</th>
                                <th>Jumlah</th>
                                <th>Harga</th>
                                <th>Gambar</th>
                                <th>Deskripsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $key => $product)
                            <tr>
                                <td> {{ $key + 1 }} </td>
                                <td> {{ $product -> name }} </td>
                                <td> {{ $product -> id }}</td>
                                <td> {{ $product -> brand }} </td>
                                <td> {{ $product -> jumlah }} </td>
                                <td> Rp. {{ $product -> harga }} </td>
                                <td> <img class="card-img-top" src="/image/{{ $product -> gambar }}" style="width:100px"> </td>
                                <td> {{ $product -> deskripsi }} </td>
                                       
                            </tr>
                             @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
       
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Data User</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table id="basic-datatables" class="display table table-striped table-hover" >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $data)
                                @if ($data -> id != 1)
                                <tr>
                                    <td> {{ $data -> id}} </td>
                                    <td> {{ $data -> name }} </td>
                                    <td> {{ $data -> email }}</td>
                                        
                                </tr>
                                @endif
                            @endforeach     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <section class="index">
    <div class="index-grid">
        <div class="index-card">
            <h3>Table Product</h3>

            <table>
                <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Brand</th>
                        <th>Harga</th>
                        <th>Foto</th>
                        <th>Activity</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Baju</td>
                        <td>Chanel</td>
                        <td>30000</td>
                        <td>-</td>
                        <td><span class="badge success">Edit</span></td>
                    </tr>
                    <tr>
                        <td>Celana</td>
                        <td>Dior</td>
                        <td>20000</td>
                        <td>-</td>
                        <td><span class="badge success">Edit</span></td>
                    </tr>
                    <tr>
                        <td>Belt</td>
                        <td>Balenciaga</td>
                        <td>100000</td>
                        <td>-</td>
                        <td><span class="badge success">Edit</span></td>
                    </tr>
                    <tr>
                        <td>Topi</td>
                        <td>HM</td>
                        <td>2233</td>
                        <td>-</td>
                        <td><span class="badge success">Edit</span></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <form>
        <div class="form-row">
          <div class="form-group col-md-5">
            <label for="inputEmail4">Email</label>
            <input type="email" class="form-control" id="inputEmail4" placeholder="Email">
          </div>
          <div class="form-group col-md-5">
            <label for="inputPassword4">Password</label>
            <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
          </div>
        </div>
          <div class="form-group col-md-5">
          <label for="inputAddress">Address</label>
          <input type="text" class="form-control" id="inputAddress" placeholder="1234 Main St">
        </div>
        <div class="form-group">
          <label for="inputAddress2">Address 2</label>
          <input type="text" class="form-control" id="inputAddress2" placeholder="Apartment, studio, or floor">
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputCity">City</label>
            <input type="text" class="form-control" id="inputCity">
          </div>
          <div class="form-group col-md-4">
            <label for="inputState">State</label>
            <select id="inputState" class="form-control">
              <option selected>Choose...</option>
              <option>...</option>
            </select>
          </div>
          <div class="form-group col-md-2">
            <label for="inputZip">Zip</label>
            <input type="text" class="form-control" id="inputZip">
          </div>
        </div>
        <div class="form-group">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
              Check me out
            </label>
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Sign in</button>
      </form>
</section> --}}
@endsection