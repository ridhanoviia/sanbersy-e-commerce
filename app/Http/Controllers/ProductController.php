<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;


class ProductController extends Controller
{
    public function index()
    {
        $product = Product::all();
        return view('product.index', compact('product'));
    }

    public function create()
    {
        return view('product.create');
    }

    public function upload(){
        $gambar = Product::get();
        return view('index',['gambar' => $gambar]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'brand' => 'required',
            'jumlah' => 'required',
            'harga' => 'required',
            'gambar' => 'mimes:jpeg,png,jpg|max:5048',
            'deskripsi' => 'required'
        ]);

        // menyimpan data file yang diupload ke variabel $file
        $gambar = $request->file('gambar');
    
        $nama_file = time()."_".$gambar->getClientOriginalName();
    
        // isi dengan nama folder tempat kemana file diupload
        $tujuan_upload = 'image';
        $gambar->move($tujuan_upload,  $nama_file);

        Product::create([
            'name' => $request->name,
            'brand' => $request->brand,
            'jumlah' => $request->jumlah,
            'harga' => $request->harga,
            'gambar' => $nama_file,
            'deskripsi' => $request->deskripsi
        ]);
        
        return redirect('/dashboard')->with('success', 'Data sukses ditambahkan');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('product.show', compact('product'));
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return view('product.edit', compact('product'));
    }

    public function update(Request $request, $id)
    {
        $nama_file = null;
        if ($request->gambar) {
            $nama_file = $request->gambar->getClientOriginalName() . '-' . time() . '.' . $request->gambar->extension();
            $request->gambar->move(public_path('image'), $nama_file);
        }

        $product = Product::where('id', $id)->update([
            "name" => $request["name"],
            "brand" => $request["brand"],
            "jumlah" => $request["jumlah"],
            "harga" => $request["harga"],
            "gambar" => $nama_file,
            "deskripsi" => $request["deskripsi"]
        ]);       
        
        return redirect('/admin/dataproduct')->with('success', 'Data sukses diupdate');
    }

    public function destroy($id)
    {
        Product::destroy($id);
        return redirect('/admin/dataproduct')->with('toast_success', 'Data sukses dihapus');
    }

    function coba()
    {
        $product = Product::all();
        return view('coba', compact('product'));
    }

    function detail($id)
    {
        $product = Product::find($id);
        return view('product.detail', compact('product'));
    }

    public function cart(Request $request, $id)
    {
        $data = Product::find($id);
        $data->jumlah = $data->jumlah - $request->qty;
        $data->update();
        return redirect('/detail/'.$id)->with('toast_success', 'Added to cart');
    }
    public function welcome()
    {
        $product = Product::all();
        return view('welcome',compact('product'));
    }
}
