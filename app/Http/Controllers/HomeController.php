<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $product = Product::all();
        return view('welcome',compact('product'));
    }
    public function index2()
    {
        $product = Product::all();
        $users = User::all();
        return view('layouts.main',compact('product','users'));
    }

    public function profile()
    {
        return view('auth.profile')->with('user', auth()->user());
    }

    public function update(Request $request)
    {
        $user = User::find(Auth::user()->id)->update([
            "name" => $request["name"],
            "email" => $request["email"]
        ]);       
        // $user = auth()->user();
        
        // $user->update([
        //     "name" => $request["name"],
        //     "email" => $request["email"],
        //     "phone" => $request["phone"]
        // ]);       
    
        return redirect('/profile')->with('success', 'Data sukses diupdate');
    }

}
